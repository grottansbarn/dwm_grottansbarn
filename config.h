/* See LICENSE file for copyright and license details. */

// sound
#include <X11/XF86keysym.h>
/* commands */
static const char *upvol[] = { "amixer", "set", "Master", "2+", NULL };
static const char *downvol[] = { "amixer", "set", "Master", "2-", NULL };
static const char *mute[] = { "amixer", "-q", "set", "Master", "toggle", NULL };

// for pulse compatible //
// static const char *upvol[] = { "amixer", "-q", "sset", "Master", "1%+", NULL };
// static const char *downvol[] = { "amixer", "-q", "sset", "Master", "1%-", NULL };
// static const char *mute[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "toggle", NULL };

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	// { "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     border size     arrange function */
	{ "[M]",      0,              monocle }, /* first entry is default */
	{ "[]=",      borderpx,       tile },
};

static const Layout layout_float = {
	"><>",        borderpx,       NULL    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define WINKEY Mod4Mask
#define ALTKEY Mod1Mask

#define TAGKEYS(KEY,TAG) \
	{ WINKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ WINKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ WINKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* commands */
static const char *termcmd[]  = { "terminator", NULL };
static const char *firefoxcmd[] = { "firefox", NULL};
static const char *cmdprintscreen[]  = { "scrot", "~/Pictures/screenshots/%Y-%m-%d-%s.jpg", NULL };

static const char **autostart[] = {
	termcmd,
	// "sh", "-c", "while :; do dwmstatus.sh -; sleep 60; done", NULL,
};

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ 0,              XF86XK_AudioRaiseVolume, spawn,          {.v = upvol } },
	{ 0,              XF86XK_AudioLowerVolume, spawn,          {.v = downvol } },
	{ 0,              XF86XK_AudioMute,        spawn,          {.v = mute } },
	{ MODKEY,                       XK_p,      dmenurun,       {0} },
	{ WINKEY,                       XK_t,      spawn,          {.v = termcmd } },
	{ WINKEY,                       XK_f,      spawn,          {.v = firefoxcmd } },
	{ 0,                            XK_Print,  spawn,          {.v = cmdprintscreen } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ ALTKEY,                       XK_Tab,    focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
    { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	//{ MODKEY,                       XK_j,      view,           {0} },
	{ WINKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[1]} },
	//{ MODKEY,                       XK_f,      setlayout,      {.v = &layout_float} },
	{ MODKEY|ShiftMask,             XK_m,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        nextlayout,     {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layout_float} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

